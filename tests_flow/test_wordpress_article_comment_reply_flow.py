import time
from base64 import b64encode
import requests
import pytest
import lorem

# GLOBAL VARIABLES
username_editor = 'editor'
password_editor = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
username_commenter = 'commenter'
author_email = 'commenter@somesite.com'
password_commenter = 'SXlx hpon SR7k issV W2in zdTb'

blog_url = 'https://gaworski.net'
posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"
comments_endpoint_url = blog_url + "/wp-json/wp/v2/comments"
token_editor = b64encode(f"{username_editor}:{password_editor}".encode('utf-8')).decode("ascii")
token_commenter = b64encode(f"{username_commenter}:{password_commenter}".encode('utf-8')).decode("ascii")


@pytest.fixture(scope='module')
def article():
    timestamp = int(time.time())
    return {
        "article_creation_date": timestamp,
        "article_title": "This is new post " + str(timestamp),
        "article_subtitle": lorem.sentence(),
        "article_text": lorem.paragraph()
    }


@pytest.fixture(scope='module')
def headers_editor():
    return {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_editor
    }


@pytest.fixture(scope='module')
def headers_commenter():
    return {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_commenter
    }


@pytest.fixture(scope='module')
def posted_article(article, headers_editor):
    payload = {
        "title": article["article_title"],
        "excerpt": article["article_subtitle"],
        "content": article["article_text"],
        "status": "publish",
        "author": 2
    }
    response = requests.post(url=posts_endpoint_url, headers=headers_editor, json=payload)
    return response


@pytest.fixture(scope='module')
def comment(article, posted_article):
    timestamp = int(time.time())
    return {
        "comment_creation_date": timestamp,
        "comment_text": lorem.paragraph(),
        "comment_author_email": author_email,
        "comment_author_name": "Viola",
        "post_id": posted_article.json()["id"]
    }


@pytest.fixture(scope='module')
def posted_comment(comment, headers_commenter):
    payload = {
        "post": comment["post_id"],
        "content": comment["comment_text"],
        "author_name": comment["comment_author_name"],
        "author_email": comment["comment_author_email"]
    }
    response = requests.post(url=comments_endpoint_url, headers=headers_commenter, json=payload)
    return response


@pytest.fixture(scope='module')
def reply(comment, posted_comment):
    timestamp = int(time.time())
    return {
        "reply_creation_date": timestamp,
        "reply_text": lorem.paragraph(),
        "reply_author_email": "editor@somesite.com",
        "parent_comment_id": posted_comment.json()["id"],
        "post_id": comment["post_id"]
    }

@pytest.fixture(scope='module')
def posted_reply(reply, headers_commenter):
    payload = {
        "post": reply["post_id"],
        "content": reply["reply_text"],
        "author": 2,
        "author_email": reply["reply_author_email"],
        "parent": reply["parent_comment_id"]
    }
    response = requests.post(url=comments_endpoint_url, headers=headers_commenter, json=payload)
    return response


def test_new_post_is_successfully_created(posted_article):
    assert posted_article.status_code == 201
    assert posted_article.reason == "Created"


def test_newly_created_post_can_be_read(article, posted_article, headers_editor):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = f'{posts_endpoint_url}/{wordpress_post_id}'
    published_article = requests.get(url=wordpress_post_url, headers=headers_editor)
    assert published_article.status_code == 200
    assert published_article.reason == "OK"
    wordpress_post_data = published_article.json()
    assert wordpress_post_data["title"]["rendered"] == article["article_title"]
    assert wordpress_post_data["excerpt"]["rendered"] == f'<p>{article["article_subtitle"]}</p>\n'
    assert wordpress_post_data["content"]["rendered"] == f'<p>{article["article_text"]}</p>\n'
    assert wordpress_post_data["status"] == 'publish'
    assert wordpress_post_data["author"] == 2


def test_new_comment_is_successfully_created(posted_comment):
    assert posted_comment.status_code == 201
    assert posted_comment.reason == "Created"


def test_comment_is_linked_to_correct_post(comment, posted_comment, headers_commenter):
    comment_id = posted_comment.json()["id"]
    comment_url = f'{comments_endpoint_url}/{comment_id}'
    published_comment = requests.get(url=comment_url, headers=headers_commenter)
    assert published_comment.status_code == 200
    assert published_comment.reason == "OK"
    comment_data = published_comment.json()
    assert comment_data["post"] == comment["post_id"]
    assert comment_data["content"]["rendered"] == f'<p>{comment["comment_text"]}</p>\n'
    assert comment_data["author_name"] == comment["comment_author_name"]


def test_new_reply_is_successfully_created(posted_reply):
    assert posted_reply.status_code == 201
    assert posted_reply.reason == "Created"

def test_reply_is_linked_to_correct_comment(reply, posted_reply, headers_commenter):
    reply_id = posted_reply.json()["id"]
    reply_url = f'{comments_endpoint_url}/{reply_id}'
    published_reply = requests.get(url=reply_url, headers=headers_commenter)
    assert published_reply.status_code == 200
    reply_data = published_reply.json()
    assert reply_data["post"] == reply["post_id"]
    assert reply_data["parent"] == reply["parent_comment_id"]
    assert reply_data["content"]["rendered"] == f'<p>{reply["reply_text"]}</p>\n'
    assert reply_data["author"] == 2